﻿using UnityEngine;
using System.Collections;

// スライダーで動かされる子要素
public class SliderChild : MonoBehaviour {

	public bool posLocked = false;
	private Slider parent;

	void Start(){
		parent = transform.parent.GetComponent<Slider>();
	}

	// ロックされていなければ親スライダーに設定された軸方向にのみ動く
	public void SetPos(Vector3 newPos){
		if(posLocked){
			return;
		}

		switch( parent.curDir ){
		case Slider.eDir.X :
			transform.position = new Vector3(newPos.x, transform.position.y, transform.position.z);
			break;
		case Slider.eDir.Y :
			transform.position = new Vector3(transform.position.x, newPos.y, transform.position.z);
			break;
		case Slider.eDir.Z :
			transform.position = new Vector3(transform.position.x, transform.position.y, newPos.z);
			break;
		}
	}
}