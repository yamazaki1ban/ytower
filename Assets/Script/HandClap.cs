﻿using UnityEngine;
using System.Collections;

public class HandClap : MonoBehaviour {


	[SerializeField]
	Transform right_palm;

	[SerializeField]
	Transform left_palm;

	const float Clap_Distance = 0.2f; 

	// 勇者
	Hero hero;

	// リスポーンポイント
	[SerializeField]
	RespawnPoint respawn;

	// Update is called once per frame
	void Update () {

		if (right_palm == null || left_palm == null) {
			return;
		}

		if(!right_palm.gameObject.activeInHierarchy || !left_palm.gameObject.activeInHierarchy){
			return;
		}

		CheckClapFlag();
	}

	/// <summary>
	/// Clap判定をする
	/// </summary>
	void CheckClapFlag(){
		float distance = (right_palm.position - left_palm.position).magnitude;
		if (distance <= Clap_Distance) {
			Clap ();
		}
		Debug.Log ("distance = " + distance.ToString ());
	}

	/// <summary>
	/// Clapする
	/// </summary>
	void Clap(){
		if (hero == null) {
			hero = respawn.hero;
		}

		hero.Jump ();

	}
}
