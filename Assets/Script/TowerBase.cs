﻿using UnityEngine;
using System.Collections;

/// <summary>
/// タワーのガワを生成
/// </summary>
public class TowerBase : MonoBehaviour {

	public GameObject partsPrefab;

	// タワー階層間の間隔
	public float interval = 10f;

	// 階数
	public int height = 5;

	// Use this for initialization
	void Start () {

		for (int i = 0; i < height; i++) {

			GameObject t = Instantiate (partsPrefab);
			if (transform.parent != null) {
				t.transform.parent = transform.parent;
			}
			t.transform.localScale = transform.localScale;

			t.transform.localPosition = transform.localPosition + (Vector3.up * interval * transform.localScale.y * i);
			t.transform.parent = transform;

			t.name = "Floor" + (i + 1).ToString ();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
