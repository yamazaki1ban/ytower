﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class HeroController : MonoBehaviour
{

    CharacterController controller;

    Vector3 moveDirection = Vector3.zero;

    //残機
    public int life = 3;

    public float speed = 1.2f;
    public float gravity;
    public float speedJump;

    AudioSource[] audioSources;


    void Start()
    {
        controller = GetComponent<CharacterController>();
		controller.enabled = true;
        audioSources = gameObject.GetComponents<AudioSource>();
    }

    void Update()
    {
        //左右移動
        moveDirection.z = Input.GetAxis("Horizontal") * speed;
        //回転


        
        if (Input.GetKeyDown("space")) ClapJump();
        moveDirection.y -= gravity * Time.deltaTime;

        Vector3 globalDirection = transform.TransformDirection(moveDirection);
		controller.Move(globalDirection * Time.deltaTime);

    }

    //手をたたいたらジャンプ(spaceで代用)
    public void ClapJump()
    {
        moveDirection.y = speedJump;
        audioSources[0].Play();
    }

    //外部からの残機取得用
    public int Life ()
    {
        return life;
    }


    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.gameObject.tag == "trap")
        {
            
            // 残機を減らす
            life--;
            Debug.Log(life);

            //悲鳴
            audioSources[1].Play();

            //ジャンプ
            moveDirection.y = speedJump;

            //仮ゲームオーバー
            if(life <= 0)
            {
                SceneManager.LoadScene("stage_edit");
            }

        }



    }
}

