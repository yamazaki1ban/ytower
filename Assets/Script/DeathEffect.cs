﻿using UnityEngine;
using System.Collections;

public class DeathEffect : MonoBehaviour {

	float lifeTime = 2f;

	AudioSource myAudio;
	[SerializeField]
	AudioClip se_die;

	// Use this for initialization
	void Start () {
		myAudio = GetComponent<AudioSource> ();
		if (se_die != null) {
			myAudio.PlayOneShot (se_die);
		} 
	}
	
	// Update is called once per frame
	void Update () {
		if (lifeTime <= 0) {
			Destroy (gameObject);
		}

		lifeTime -= Time.deltaTime;
	}
}
