﻿using UnityEngine;
using System.Collections;

// 特定の一方向にだけスライドできる仕掛け
public class Slider : MonoBehaviour {

	// スライダーの動く方向
	public enum eDir{
		X,
		Y,
		Z,
	};
	[SerializeField]
	public eDir curDir = eDir.X;

	// 掴んで動かせる子要素
	[SerializeField]
	SliderChild child;

	// 子要素が動かせる範囲を定義するレール
	private Transform rail;

	// 動かせる範囲（絶対値）
	private float maxRange;

	// 子要素の初期位置
	private Vector3 originChildPos = Vector3.zero;

	// Use this for initialization
	void Start () 
	{	
		rail = transform.FindChild("Rail").transform;

		// レールのスケールで子要素の可動範囲を定義
		switch( curDir ){
		case eDir.X:
			maxRange = rail.transform.localScale.x / 2;
			break;
		case eDir.Y:
			maxRange = rail.transform.localScale.y / 2;
			break;
		case eDir.Z:
			maxRange = rail.transform.localScale.z / 2;
			break;
		}

		// 子要素の初期位置を設定
		if( child ){
			originChildPos = child.transform.localPosition;
		}

	}
	
	// Update is called once per frame
	void Update () {
		ManageChildPos();
	}

	// 子要素の位置を制御
	private void ManageChildPos(){

		float pos = 0;

		// 特定の軸の位置を取得
		switch( curDir ){
		case eDir.X:
			pos = child.transform.localPosition.x;
			break;
		case eDir.Y:
			pos = child.transform.localPosition.y;
			break;
		case eDir.Z:
			pos = child.transform.localPosition.z;
			break;
		}

		// 可動を範囲外にいるときはポジションをロック
		child.posLocked = Mathf.Abs( pos ) > maxRange;

		// 可動範囲内に補正した値で該当の軸の座標を上書き
		pos = Mathf.Clamp( pos, -maxRange, maxRange );
		switch( curDir ){
		case eDir.X:
			child.transform.localPosition = new Vector3( pos, originChildPos.y, originChildPos.z );
			break;
		case eDir.Y:
			child.transform.localPosition = new Vector3( originChildPos.x, pos, originChildPos.z );
			break;
		case eDir.Z:
			child.transform.localPosition = new Vector3( originChildPos.x, originChildPos.y, pos );
			break;
		}
	}
}