﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RespawnPoint : MonoBehaviour {

	public Hero hero{
		get{
			if (childOnStage == null) {
				return null;
			}

			return childOnStage;
		}
	}

	public GameObject child;

	//ステージ
	public FallingStage stage;

	// マグマ
	public Transform lava;

	// 現在生成されている勇者
	Hero childOnStage = null;

	// 勇者が死んでから生成するまでの待ち時間
	const float Respawn_wait = 1f;
	float respawn_timer = 0;

	// Awakeと同時に勇者を生成するか？
	public bool CreateOnAwake = true;

	// 勇者を生成できる状態か
	bool readyToCreateHero = true;

	// チェックポイント到達時にマグマからどのくらいの高度までステージを下降させるか
	public float offsetToLava = -0.14f;

	// 依存しているチェックポイント
	public CheckPoint depended;

	void Awake () {
		if (CreateOnAwake) {
			StartCoroutine( CreateChild () );
		}

		if (depended != null) {
			depended.SetDelegate (DependedCheckPointIsDestoyed);
		}

		readyToCreateHero = SceneManager.GetActiveScene().name == "Main";
	}


	/// <summary>
	/// 勇者を生成する
	/// </summary>
	IEnumerator CreateChild(){
		
		while(respawn_timer > 0) {
			respawn_timer -= Time.deltaTime;
			yield return null;
		}

		while(childOnStage != null) {
			yield return null;
		}
		// クリア済みだったら生成しない
		if (!GameManager.isClear && readyToCreateHero) {	
			Debug.Log ("勇者を生成します");
			childOnStage = Instantiate (child).GetComponent<Hero> ();
			childOnStage.transform.parent = transform;
			childOnStage.transform.localScale = transform.localScale;
			childOnStage.transform.position = transform.position;
			childOnStage.SetDelegate (DeadChildOnStage, ReachCheckPoint);
		}
	}


	/// <summary>
	/// ステージにいた勇者が死んだ
	/// </summary>
	void DeadChildOnStage(){

		if (!readyToCreateHero) {
			GameObject.Find ("GameManager").GetComponent<GameManager>().GameOver();

			return;
		}

		respawn_timer = Respawn_wait;

		StartCoroutine( CreateChild () );
	}

	/// <summary>
	/// ステージにいた勇者がチェックポイントに到達した
	/// </summary>
	void ReachCheckPoint(Transform checkPoint){

		Debug.Log("チェックポイントに到達");

		// 勇者生成可能フラグを立てる
		readyToCreateHero = true;

		// リスポーン位置をチェックポイントの位置に移動
		transform.position = checkPoint.position + Vector3.up * 0.05f;

		// 勇者の位置を再設定
		childOnStage.transform.position = transform.position;

		// ステージをこのチェックポイントの位置まで下降させる
		FallStageToLavaHeight();

		// 前の依存チェックポイントのイベントを削除
		if(depended != null){
			depended.SetDelegate (null);
		}
		// チェックポイントに依存する
		depended = checkPoint.GetComponent<CheckPoint>();
		depended.SetDelegate (DependedCheckPointIsDestoyed);
	}

	/// <summary>
	/// 依存していたチェックポイントが破壊された
	/// </summary>
	void DependedCheckPointIsDestoyed(){

		// これ以上勇者を生成しない。
		readyToCreateHero = false;

	}

	/// <summary>
	/// マグマの水位までステージを下降させる
	/// </summary>
	void FallStageToLavaHeight(){

		// このチェックポイントとマグマの距離
		float distance = transform.position.y - lava.transform.position.y;
	
		stage.Down(distance + offsetToLava);
	}
}
