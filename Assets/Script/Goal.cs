﻿using UnityEngine;
using System.Collections;

/// <summary>
/// これに触るとゲームクリア
/// </summary>
public class Goal : MonoBehaviour {

	Collider m_col;

	void Start(){
		m_col = GetComponent<Collider> ();
	}

	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag.Equals ("Hero")) {

			// 複数回触れなくする
			m_col.enabled = false;

			// ゴール通知
			col.gameObject.GetComponent<Hero> ().Goal ();
		}
	}
}
