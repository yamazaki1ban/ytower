﻿/**/
using UnityEngine;
using System.Collections;
public class DarkScreen : MonoBehaviour {

	MeshRenderer mesh;

	void Start(){
		mesh = GetComponent<MeshRenderer> ();
	}
	// フェード処理が完了したときに呼ぶデリゲート
	public delegate void cbFadeComplete();
	public IEnumerator FadeScreen(float end, cbFadeComplete cb, float waitBeforeFade=0f, float waitAfterComplete=0f)
	{
		float alpha = mesh.material.color.a;
		Color newColor;

		yield return new WaitForSeconds ( waitBeforeFade );

		// １秒毎の減衰率 １に設定すると１秒でフェード完了
		float rate = 0.25f;
		bool fadeout = end > alpha;
		if(fadeout){
			while(alpha <= end){
				alpha += rate * Time.deltaTime;
				newColor = new Color (0, 0, 0, alpha);
				mesh.material.color = newColor;
				yield return null;
			}
		}else{
			while(alpha >= end){
				alpha -= rate * Time.deltaTime;
				newColor = new Color (0, 0, 0, alpha);
				mesh.material.color = newColor;
				yield return null; 
			}
		}

		yield return new WaitForSeconds (waitAfterComplete);

		if( cb != null ){
			cb();
		}
	}
}