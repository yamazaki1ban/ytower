﻿using UnityEngine;
using System.Collections;
using Leap;

namespace Leap.Unity {

	public class MyHandClass : MonoBehaviour {

		public enum eState{
			HOLD,
			SLIDE,
			DEFAULT,
		};

		private eState curState = eState.DEFAULT;

		public const float TRIGGER_DISTANCE_RATIO = 0.65f;

		/** The stiffness of the spring force used to move the object toward the hand. */
		public float forceSpringConstant = 100.0f;

		/** アイテムをつかめる最大距離*/
		public float holdDistance = 2.0f;

		// 指がpinch状態（親指と他の指でつまんでいる状態）か？
		protected bool pinching_;

		// 掴んでいるオブジェクト
		protected Collider grabbed_;

		public Collider grabbed{
			get{
				return grabbed_;
			}
		}

		public delegate void delegateFunc(Collider col);
		public delegateFunc m_releaseFunc;
		public delegateFunc m_pinchFunc;

	  void Start() {
	    pinching_ = false;
	    grabbed_ = null;
	  }

	  /** pinch 開始 */
	  void OnPinch(Vector3 pinch_position) {
	    pinching_ = true;

	    // つかめる範囲にあるオブジェクトのコライダ
	    Collider[] close_things = Physics.OverlapSphere(pinch_position, holdDistance);
	    Vector3 distance = new Vector3(holdDistance, 0.0f, 0.0f);

		// 一番近いオブジェクトを掴み中オブジェクトに登録
	    for (int j = 0; j < close_things.Length; ++j) {
	      Vector3 new_distance = pinch_position - close_things[j].transform.position;
	      if(
				close_things[j].GetComponent<Rigidbody>() != null 
				&& new_distance.magnitude < distance.magnitude 
				&& !close_things[j].transform.IsChildOf(transform)
			){
				// "MovableParts"タグのアイテムだけつかめる
				if( close_things[j].gameObject.tag.Equals("MovableParts") ){
					Debug.Log( close_things[j].gameObject.name + "を掴んだ");
					grabbed_ = close_things[j];
					distance = new_distance;	
				}else{
					Debug.Log( close_things[j].gameObject.name + "はつかめない tag=" + close_things[j].gameObject.tag);
				}
	      }
	    }

			// 掴んでいる対象によってステート切り替え
		if( grabbed_ != null){
			MovableParts mp = grabbed_.GetComponent<MovableParts>();
			if( mp != null ){ 
					// 掴み移動モード
					curState = eState.HOLD;

				mp.rig.constraints = RigidbodyConstraints.None;
				}else{
					//掴みずらしモード
					curState = eState.SLIDE;
				}
		}

		if(close_things.Length == 0){
			Debug.Log("つかめる範囲になにもない");
		}
	  }

	  /** Pinch解除 */
	void OnRelease() {
		Debug.Log("離した");
		if( m_releaseFunc != null){
			m_releaseFunc(grabbed_);
		}

		grabbed_ = null;
		pinching_ = false;

		// モード解除
		curState = eState.DEFAULT;

	}

	  /**
	   * Checks whether the hand is pinching and updates the position of the pinched object.
	   */
	  void Update() {
			
	    bool trigger_pinch = false;
		
		CapsuleHand hand_cont= GetComponent<CapsuleHand>();
		Hand leap_hand = hand_cont.GetLeapHand ();
		if (leap_hand == null){
			return;
		}

	    
		// Scale trigger distance by thumb proximal bone length.
	    Vector leap_thumb_tip = leap_hand.Fingers[0].TipPosition;
	    float proximal_length = leap_hand.Fingers[0].Bone(Bone.BoneType.TYPE_PROXIMAL).Length;
	    float trigger_distance = proximal_length * TRIGGER_DISTANCE_RATIO;

	    // Check thumb tip distance to joints on all other fingers.
	    // If it's close enough, start pinching.
	    for (int i = 1; i < HandModel.NUM_FINGERS && !trigger_pinch; ++i) {
	    Finger finger = leap_hand.Fingers[i];

	    for (int j = 0; j < FingerModel.NUM_BONES && !trigger_pinch; ++j) {
	        Vector leap_joint_position = finger.Bone((Bone.BoneType)j).NextJoint;
	        if (leap_joint_position.DistanceTo(leap_thumb_tip) < trigger_distance)
	        	trigger_pinch = true;
	    	}
	    }

		Vector3 pinch_position = leap_hand.Fingers[0].TipPosition.ToVector3();

	    // Only change state if it's different.
		if (trigger_pinch && !pinching_){
			OnPinch(pinch_position);
		}else if (!trigger_pinch && pinching_){
			OnRelease();
		}

	    // Accelerate what we are grabbing toward the pinch.
	    if (grabbed_ != null) {
				
	      //Vector3 distance = pinch_position - grabbed_.transform.position;
	      //grabbed_.GetComponent<Rigidbody>().AddForce(forceSpringConstant * distance);
			
			switch(curState){
			case eState.HOLD:
				grabbed_.transform.position = pinch_position;
				break;
			case eState.SLIDE:
				
				grabbed_.transform.GetComponent<SliderChild>().SetPos( pinch_position );
				break;
			}

	    }
	  }
	}
}
