﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckPoint : MonoBehaviour {

	private ParticleSystem ps;
	private GameObject efs_die;
	private eventDelegate destroyDelegate;
	public TextMesh textMeshPrefab;

	// イベント
	public delegate void eventDelegate();

	static List<CheckPoint> checkPoints = new List<CheckPoint>();
	// Use this for initialization
	void Start () {
		ps = transform.FindChild("Effect").GetComponent<ParticleSystem> ();
		efs_die = transform.FindChild("Effect_fire").gameObject;
		efs_die.SetActive (false);

		checkPoints.Add (this);
	}

	/// <summary>
	/// Destroy時のイベントをセット
	/// </summary>
	public void SetDelegate(eventDelegate delegateFunc){
		destroyDelegate = delegateFunc;
	}

	/// <summary>
	/// 勇者の復活元として設定されているか？
	/// </summary>
	public bool activated{
		set{ 
			_activated = value;

			// アクティブになった自身はパーティクルを大きく、それ以外は小さく
			if (ps != null) {
				ps.startSize = value ? 0.02f : 0.005f;
			}

			if (!value) {
				return;
			}

			// アクティブにした場合はほかのチェックポイントを無効化
			foreach (CheckPoint c in checkPoints) {
				if (c.Equals (this)) {
					// チェックポイントテキストメッシュを表示する
					StartCoroutine( Displaytext() );
					continue;
				}

				// 自身以外は非アクティブ
				c.activated = false;
			}
		}

		get{
			return _activated;
		}
	}
	private bool _activated = false;

	void OnTriggerEnter(Collider col){
		if (col.tag.Equals ("DeadZone")) {

			if (destroyDelegate != null) {
				destroyDelegate ();
			}

			Die ();
		}
	}

	/// <summary>
	/// 破壊演出
	/// </summary>
	void Die(){

		ps.gameObject.SetActive (false);
		efs_die.SetActive (true);
		GetComponent<AudioSource> ().Play ();

		Destroy (gameObject, 3f);
	}

	IEnumerator Displaytext(){

		float spd = 0.75f;
		float alpha = 1;
		float dur = 8f;
		TextMesh t = Instantiate( textMeshPrefab );
		t.transform.parent = transform;
		t.transform.localScale = Vector3.one * 0.03f;
		t.transform.localPosition = Vector3.zero;

		t.text = "check point";


		// 上昇しつつ透過
		while(alpha > 0){
			if (t.transform.localPosition.y < 1.5f) {
				t.transform.localPosition = new Vector3 (t.transform.localPosition.x, t.transform.localPosition.y + spd * Time.deltaTime);
			}
			t.color = new Color(1,1,1, alpha);
			alpha -= Time.deltaTime / dur;

			yield return null;
		}

		Destroy( t.gameObject );
	}
}
