﻿using UnityEngine;
using System.Collections;

public class Socket : MonoBehaviour {

	enum eStaus{
		Idle, 		// 通常状態
		Focused,	// パーツを持った手にロックオンされた状態
		Default,
	}
	private eStaus m_status;

	// セットされているパーツ
	private Transform m_parts;

	// 
	private Leap.Unity.MyHandClass m_hand;

	// 自身の色を設定
	private Material[] mat;

	// Use this for initialization
	void Start () {
		m_status = eStaus.Default;
	}
	
	// Update is called once per frame
	void Update () {
	}

	private void OnTriggerEnter(Collider col){

		switch( col.gameObject.tag ){

		case "MovableParts":
			break;
		case "Hand":

			Debug.Log("手が範囲内に侵入した");

			if( this.m_parts == null){
				m_hand = col.transform.parent.GetComponent<Leap.Unity.MyHandClass>();

				// 手を開いた時のイベントを登録
				m_hand.m_releaseFunc = ReceiveParts; 

				// 手がパーツを持っていたら自身の色をロックオンされ中に変更
				if( m_hand.grabbed != null){
					UpdateColor(eStaus.Focused);
				}
			}else{
				Debug.Log("すでにパーツがセットされているのでスルー");
			}

			break;

		default:
			break;
		}
	}		
		
	private void OnTriggerExit(Collider col){

		switch( col.gameObject.tag ){

		case "MovableParts":
			break;
		case "Hand":
			Debug.Log(" 手が範囲外に移動した ");

			// 手を開いた時のイベントを空にする
			m_hand.m_releaseFunc = null; 

			// 手がパーツを持っていたらパーツへの参照を空にする
			if( m_hand.grabbed != null){
				m_parts = null;
			}

			// 自身の色を基本色に変更
			UpdateColor(eStaus.Idle);
			break;

		default:
			break;
		}
	}	


	/// <summary>
	/// ステータスによって自身の色を変更
	/// </summary>
	private void UpdateColor(eStaus newStatus){

		Color newColor = Color.white;

		switch(newStatus){

		case eStaus.Idle:
			newColor = Color.white;
			break;
		case eStaus.Focused:
			newColor = Color.red;
			break;
		default:
			break;
		}


		foreach( Renderer m in transform.GetComponentsInChildren<Renderer>() ){
			m.material.color = newColor;
		}
	}

	/// <summary>
	/// 手からパーツを受け取る
	/// </summary>
	private void ReceiveParts(Collider col ){
		if(col == null){
			return;
		}

		// 受け取った参照をメンバ変数に登録
		m_parts = col.transform;

		// 位置と回転を調整して手による操作以外動かなくする
		m_parts.rotation = Quaternion.identity;
		m_parts.position = this.transform.position;
		m_parts.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

		Debug.Log("パーツをソケットにセットした");
	}

}