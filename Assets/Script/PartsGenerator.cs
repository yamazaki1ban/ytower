﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PartsGenerator : MonoBehaviour {

	float interval = 5f;
	int maxChildNum = 15;
	float timer = 0;
	float maxDiff = 1.5f;

	[SerializeField]
	GameObject parts;

	List<GameObject> m_parts = new List<GameObject>();

	// Use this for initialization
	void Start () {
		timer = interval;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;

		if(timer <= 0 ){
			if(m_parts.Count >= maxChildNum){
				return;
			}
				
			GameObject g = Instantiate( parts);
			g.transform.position = transform.position + new Vector3( Random.Range(-maxDiff, maxDiff), 0, Random.Range(-maxDiff, maxDiff) );
			m_parts.Add( g );

			timer = interval;

		}
	}
}
