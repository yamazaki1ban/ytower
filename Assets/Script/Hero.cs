﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

/// <summary>
/// 勇者
/// </summary>
public class Hero : MonoBehaviour
{
	enum eState{
		Walk,
		Jump,
		Rura
	};
	private eState curState;

	// 勇者の移動
    public float walkSpeed = 1.2f;
    Vector3 dir = Vector3.right;
    float rotY = 90;

	// ジャンプ関連
	private Vector3 jump_velocity = Vector3.zero;
	public float jump_force = 100f;
	private const float jumpWait = 1f;
	private float jumpTimer = 0;
	Rigidbody rig;

	// SE
	AudioSource myAudio;

	// ジャンプ
	public AudioClip se_jump;

	// 呪文詠唱
	public AudioClip se_spell;

	// 移動魔法発動
	public AudioClip se_rura;

	// 移動魔法完了
	public AudioClip se_gone;

	// チェックポイント通貨
	public AudioClip se_check;

	// イベント関数
	public delegate void eventDelegate();

	// オブジェクト付きイベント関数
	public delegate void eventDelegateWithObject(Transform target);
	// 死んだときのイベント
	private eventDelegate deadDelegate;
	// チェックポイントに到達したときのイベント
	private eventDelegateWithObject reachCheckPointDelegate;
	[SerializeField]
	public DeathEffect death;

	private Animator anim;

    // Use this for initialization
    void Start()
    {
		rig = GetComponent<Rigidbody> ();
		myAudio = GetComponent<AudioSource>();
		anim = transform.FindChild("knight").GetComponent<Animator> ();
    }

    // Update is called once per frame
    void Update()
    {

		switch (curState) {
		case eState.Walk:
			
			transform.position += dir * walkSpeed * Time.deltaTime;
			transform.rotation = Quaternion.Euler (0, rotY, 0);

			if (Input.GetKeyDown (KeyCode.Space)) {
				Die ();
			}
			break;

		case eState.Jump:
			ReduceJumpWait (Time.deltaTime);
			goto case eState.Walk;

		case eState.Rura:
			break;

		default:
			break;
		}

    }

    /// <summary>
    /// なにかのトリガーに接触
    /// </summary>
    void OnTriggerEnter(Collider col)
    {

        switch (col.tag)
        {
		case "CheckPoint":
			// チェックポイント
			CheckPoint c = col.GetComponent<CheckPoint> ();
			if( !c.activated ){
				if (reachCheckPointDelegate != null) {
					reachCheckPointDelegate (col.transform);
					myAudio.PlayOneShot (se_check);
					c.activated = true;
				}
			}
			break;
            case "Socket":
                return;
		case "Hand":
		case "FixedParts":
		case "MovableParts":
			//Debug.Log ("方向転換した");
			dir = -dir;
			rotY = dir.x >= 0 ? 90 : -90;
			return;
		default:
                break;
        }
    }

	/// <summary>
	/// ジャンプ再使用時間状態だったらジャンプ
	/// </summary>
	public void Jump(){
		if (jumpTimer > 0) {
			Debug.Log ("ジャンプ再使用時間が経過していない");
			return;
		}

		rig.AddForce (Vector3.up * jump_force);
		jumpTimer = jumpWait;

		if (se_jump != null) {
			myAudio.PlayOneShot (se_jump);
		}

		curState = eState.Jump;
	}
	/// <summary>
	/// ジャンプの再使用可能時間を減少させる
	/// </summary>
	private void ReduceJumpWait(float deltaTime){
		if (jumpTimer > 0) {
			jumpTimer -= deltaTime;
			if (jumpTimer <= 0) {
				Debug.Log ("ジャンプ再使用可能");
				curState = eState.Walk;
			}
		}
	}

	/// <summary>
	/// イベントをセット
	/// </summary>
	public void SetDelegate(eventDelegate deadDelegateFunc, eventDelegateWithObject reachCheckPointDelegateFunc){
		deadDelegate = deadDelegateFunc;
		reachCheckPointDelegate = reachCheckPointDelegateFunc;
	}


	/// <summary>
	/// 死亡
	/// </summary>
	public void Die()
	{
		GameManager.deathCount++;

		// 死亡演出
		Instantiate (death, transform.position, Quaternion.Euler(-90f,0,0));

		// 死亡時イベントがあるときは実行
		if (deadDelegate != null) {
			deadDelegate ();
		}	

		Destroy (gameObject);
	}

	/// <summary>
	/// ゲームクリア
	/// </summary>
	public void Goal(){
		GameObject.Find ("GameManager").GetComponent<GameManager>().Clear (this);
		curState = eState.Rura;
	}

	/// <summary>
	/// ゲームクリア演出
	/// </summary>
	public IEnumerator Rura(){

		// 喜びアニメ
		anim.SetTrigger("Win");

		// リジッドボディとコライダを無効化
		Destroy( rig );
		GetComponent<Collider> ().enabled = false;

		yield return new WaitForSeconds (3);
		myAudio.PlayOneShot (se_spell);

		yield return new WaitForSeconds (1.5f);
		myAudio.PlayOneShot (se_rura);

		transform.DOMove (transform.position + Vector3.up * 1f, 1f).OnComplete(delegate {
			myAudio.PlayOneShot( se_gone ) ;

			// ポリゴンモデルを無効化
			transform.FindChild("knight").gameObject.SetActive( false );

			// 数秒後に削除（すぐ削除するとSEが再生されない）
			//Destroy( gameObject, 2f );

		//	GameObject.Find ("GameManager").GetComponent<GameManager>().BackToTitle();

		});

		Debug.Log ("勇者消滅");

		yield return new WaitForSeconds (1f);

		yield break;
	}
}
