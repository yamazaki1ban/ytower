﻿using UnityEngine;
using System.Collections;

public class BreakableParts : MonoBehaviour {

	// 初期耐久値
	public int hp_init = 3;

	// 現在の耐久値
	int hp_cur;

	// ダメージを受けてから次のダメージを受けるまでの無効時間秒
	float inv_dur = 1f;

	// 無敵時間タイマー
	float inv_timer = 0f;

	// 壊れる前のモデル
	GameObject model;

	//　壊れた後のモデル
	GameObject dead;

	// Use this for initialization
	void Start () {
		model = transform.FindChild ("Model").gameObject;
		dead = transform.FindChild ("Dead").gameObject;
		dead.SetActive (false);

		hp_cur = hp_init;
	}

	void Update(){
		if (inv_timer > 0) {
			inv_timer -= Time.deltaTime;
		}
	}

	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag.Equals ("Hand")) {

			// 無敵時間はなにもしない
			if(inv_timer > 0){
				return;
			}

			ApplyDamage ();
		}
	}

	/// <summary>
	/// 耐久値を減少させる
	/// </summary>
	private void ApplyDamage(int val=1){

		Debug.Log ("Hit!");

		hp_cur -= val;

		if (hp_cur <= 0) {
			Die ();
		} else {
			inv_timer = inv_dur;
		}
	}

	/// <summary>
	/// このオブジェクトを破壊
	/// </summary>
	private void Die(){
		Destroy (gameObject, 3f);

		model.SetActive (false);
		dead.SetActive (true);

		GetComponent<BoxCollider> ().enabled = false;
		GetComponent<AudioSource> ().Play ();
	}
}
