﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

/// <summary>
/// 徐々に沈んでいくステージ
/// </summary>
public class FallingStage : MonoBehaviour {

	// 沈む速度
	public float ascendSpeed = 0.1f;

	public bool collapsing = false;
	// Update is called once per frame
	void Update () {

		// ゲームオーバーまたはクリア状態のときは下降を停止
		if (GameManager.isGameOver) {
			return;
		}

		if (!collapsing) {
			return;
		}

		// 下降させる
		transform.position += Vector3.up * ascendSpeed * Time.deltaTime;
	}

	/// 指定の高度まで一気に下降させる
	public void Down(float val){
		float newPos = transform.position.y - val;
		Debug.Log ("目標高度は" + newPos.ToString () + ", 自分の現在高度は" + transform.position.y);
		transform.DOMoveY (newPos, 4f);
	}

	public IEnumerator Collapse()
	{
		Debug.Log ("塔が崩壊");
		// 沈み切った状態の塔のポジション
		float end = -3.3f;
	
		// 下降速度大幅アップ
		ascendSpeed = -0.3f;

		// 沈み切るまでくりかえす
		while (transform.position.y > end) {
			yield return null;
		}

		yield break;
	}
}