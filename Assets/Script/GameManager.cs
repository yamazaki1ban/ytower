﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public AudioClip clear_se;

	public DarkScreen dark;

	// テキストメッシュ
	public TextMesh textMeshPrefab;

	// 塔までの距離
	float distanceFromGoal;

	// ゲーム開始からの時間
	float timer = 0;
	bool stopTimer = true;

	// 勇者死亡回数
	public static int deathCount = 0;

	// 到達高度
	float score = 0;

	public FallingStage structure;

	public GameObject effect_fire;

	Hero hero;

	AudioSource myAudio;

	void Awake(){
		myAudio = GetComponent<AudioSource> ();
		isGameOver = false;
		isClear = false;
		deathCount = 0;

		ShowGameStartSequence ();
	}

	void Update(){
		if (!stopTimer) {
			timer += Time.deltaTime;
		}

		if (Input.GetKey (KeyCode.Q)) {
			SceneManager.LoadScene (SceneManager.GetActiveScene().name);
		}

		if (Input.GetKey (KeyCode.T)) {
			SceneManager.LoadScene ("Title");
		}
	}

	// ゲームオーバーか？
	public static bool isGameOver{ get; set; }
	public static bool isClear{ get; set; }

	// クリア
	public void Clear(Hero heroInstance){
		myAudio.PlayOneShot ( clear_se );
		isClear = true;
		stopTimer = true;
		StartCoroutine( FadeBgm (0, 2f) );

		hero = heroInstance;
		// クリア演出
		StartCoroutine( ShowEnding () );
	}

	// ゲームオーバー
	public void GameOver(){
		Debug.LogWarning ("ゲームオーバー！");
		isGameOver = true;
		StartCoroutine (ShowGameOver());
	}

	/// <summary>
	/// BGMをフェード
	/// </summary>
	private IEnumerator FadeBgm(float endVolume, float wait)
	{
		yield return new WaitForSeconds (wait);

		DOTween.To (() =>myAudio.volume, x => myAudio.volume = x, endVolume, 3); 
	}
		
	/**
	* ゲーム開始演出
	*/
	void ShowGameStartSequence()
	{
		// フェードイン

		//勇者が宝箱に近づく

		// 宝箱開く

		// 勇者がルーラ

		// 勇者が天井に頭をぶつける

		// 効果音

		// 塔が沈み始める
		structure.collapsing = true;

		// ゲーム開始
		stopTimer = false;
	}

	/**
	* ゲームクリア演出
	*/
	IEnumerator ShowEnding()
	{
		// ステージパーツを壊す
		DestroyStageParts();

		// 勇者が空に消える
		yield return StartCoroutine( hero.Rura() );

		string timeStr = "";
		int min = (int)(timer / 60);
		int sec = (int)(timer % 60);

		timeStr = string.Format ("{0:D2}", min) + ":" + string.Format ("{0:D2}", sec);

		// 塔の壁で隠れていた部分にクリアロゴとクリアタイムと死亡した勇者数を表示
		TextMesh mesh = Instantiate( textMeshPrefab ).GetComponent<TextMesh>();
		mesh.text = "congraturations!!\nTIME " + timeStr + "\ndeath " + deathCount.ToString();
		mesh.transform.parent = GameObject.Find ("Stage").transform;
		mesh.transform.localPosition = new Vector3(0, 10f, 50f);
		mesh.transform.localScale = Vector3.one;
		mesh.fontSize = 100;

		// 塔がすべて崩れる
		yield return StartCoroutine( structure.Collapse() );

		// フェードアウトでタイトルに戻る
		StartCoroutine(dark.FadeScreen(1, ExecBackToTitle, 5f, 2f));
	}

	/**
	* ゲームオーバー演出
	*/
	IEnumerator ShowGameOver()
	{
		// 画面が薄暗くなる
		yield return StartCoroutine(dark.FadeScreen(0.5f, null));

		// ゴールまでの距離をテキストメッシュで表示
		TextMesh mesh = Instantiate(textMeshPrefab);
		mesh.text = score.ToString();
		mesh.transform.parent = GameObject.Find ("Stage").transform;
		mesh.transform.localPosition = new Vector3(0, 5f, 20f);
		mesh.transform.localScale = Vector3.one;
		mesh.fontSize = 50;
		mesh.text = "Game over";

		// フェードアウトでタイトルに戻る
		StartCoroutine(dark.FadeScreen(1, ExecBackToTitle, 7f, 2f));
	}

	public void BackToTitle(){
		StartCoroutine( dark.FadeScreen( 1,  ExecBackToTitle, waitAfterComplete:2f) );
	}

	private void ExecBackToTitle()
	{
		SceneManager.LoadScene("Title");
	}

	private void DestroyStageParts(){
		GameObject[] parts; 

		string[] names = new string[]{ "FixedParts", "BreakableParts", "CheckPoint" }; 

		for (int i = 0; i < names.Length; i++) {
			parts = GameObject.FindGameObjectsWithTag (names[i]);

			foreach (GameObject g in parts) {
				GameObject e = Instantiate (effect_fire);
				e.transform.parent = g.transform;
				e.transform.localPosition = Vector3.zero;
				e.transform.localScale = Vector3.one;

				Destroy (g, 3f);
			}
		}
	}
}