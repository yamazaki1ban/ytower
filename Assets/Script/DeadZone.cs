﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 侵入した勇者を消滅させる
/// </summary>
public class DeadZone : MonoBehaviour {

	void OnTriggerEnter(Collider col){
		if (col.tag.Equals ("Hero")) {
			Debug.Log (gameObject.name + "が勇者を倒した");
			col.GetComponent<Hero>().Die();
		}
	}

}
